﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaProductos
{
    public class Camion
    {
        private Sector _sectorFragil;
        private Sector _sectorNormal;
        private List<Producto> _rechazados;

        public List<Producto> ProductosRechazados
        {
            get { return _rechazados; }
            set { _rechazados = value; }
        }
        
        public Sector SectorNormal
        {
            get { return _sectorNormal; }
            set { _sectorNormal = value; }
        }
        

        public Sector SectorFragil
        {
            get { return _sectorFragil; }
            set { _sectorFragil = value; }
        }

        public Camion()
        {
            this._rechazados = new List<Producto>();
        }

        public bool bienCargado()
        {
            
            //
            return false;
        }
        
    }
}
