﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaProductos
{
    public abstract class Producto
    {
        private string _nombre;
        private float _peso;
        private float _volumen;

        public float VolumenProducto
        {
            get { return _volumen; }
            set { _volumen = value; }
        }
        

        public float PesoProducto
        {
            get { return _peso; }
            set { _peso = value; }
        }
        

        public string NombreProducto
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public Producto() 
        {
        
        }

        public Producto(string nombreProducto, float pesoProducto, float volumenProducto)
        {
            this.NombreProducto = nombreProducto;
            this.PesoProducto = pesoProducto;
            this.VolumenProducto = volumenProducto;

        }

        public abstract void ubicateEn(Camion camion);
        
    }
}
