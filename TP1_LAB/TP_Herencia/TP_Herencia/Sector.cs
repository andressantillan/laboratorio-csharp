﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaProductos
{
    public class Sector
    {
        private float _volumenMax;
        private float _pesoMax;
        private List<Producto> _productos;

        public List<Producto> Productos
        {
            get { return _productos; }
            set { _productos = value; }
        }
        

        public float PesoMax
        {
            get { return _pesoMax; }
            set { _pesoMax = value; }
        }
        

        public float VolumenMax
        {
            get { return _volumenMax; }
            set { _volumenMax = value; }
        }

        public Sector()
        {
            this._productos = new List<Producto>();
        }

        public void agregar(Producto producto)
        {
            this.Productos.Add(producto);
        }
         
    }
}
