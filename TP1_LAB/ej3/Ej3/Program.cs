﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Promedio
{
    class Program
    {

        const int MAX = 5;

        static void ingresarNumeros(int[] num) {
            for (int i = 0; i < MAX; i++)
            {
                Console.Out.WriteLine("Ingrese un numero:");
                num[i] = Convert.ToInt32(Console.ReadLine());
            }
        }

        static int calcularPromedio(int[] num) {

            int aux = 0;
            for (int i = 0; i < MAX;  i++){
                aux += num[i];
            }

            int prom = aux / MAX;

            return prom;
        }
        
        static void Main(string[] args)
        {
            int[] numeros = new int[MAX];
            ingresarNumeros(numeros);
            int prom = calcularPromedio(numeros);
            Console.Out.WriteLine("El promedio es: {0}", prom);
            Console.ReadKey();
        }
    }
}
