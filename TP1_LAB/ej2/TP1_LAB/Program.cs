﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TablaMultiplicar
{
    class Program
    {
        const int MAX = 10;
        
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Ingrese un número para listar su tabla de multiplicar: ");
            int x = Convert.ToInt32(Console.ReadLine());

            if (x > 0)
                tablaMultiplicar(x);
            else
                Console.Out.WriteLine("Ingrese un numero mayor a 0");

            Console.ReadKey();
        }

        static void tablaMultiplicar(int x) {
            int aux = 0;
            for (int i = 1; i <= MAX; i++)
            {
                aux = x * i;
                Console.Out.WriteLine("{0}x{1} = {2}", x, i, aux);
            }
        }

    }
}
