﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContadorVocales
{
    class ContadorVocales
    {
        const int MAX = 5;

        struct CantVocales
        {
            public int a;
            public int e;
            public int i;
            public int o;
            public int u;
        }

        static bool esVocal(char c) {

            return (c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u');
        }

        static void Vocales(string[] cadena, ref CantVocales vocales)
        {
            for (int i = 0; i < MAX; i++)
            {
                contarVocales(cadena[i], ref vocales);
            }
        }

        static void contarVocales(string x, ref CantVocales v)
        {
            for (int i = 0; i < x.Length; i++)
            {
                
                if (x[i] == 'a' || x[i] == 'A')
                    v.a++;

                if (x[i] == 'e' || x[i] == 'E')
                    v.e++;

                if (x[i] == 'i' || x[i] == 'I')
                    v.i++;

                if (x[i] == 'o' || x[i] == 'O')
                    v.o++;

                if (x[i] == 'u' || x[i] == 'U')
                    v.u++;

            }

        }
        
        static void Main(string[] args)
        {
            string[] s = new string[MAX];
            CantVocales cantVocales = new CantVocales();

            Console.Out.WriteLine("Ingrese varias lineas de texto:");
            for(int i=0; i < MAX; i++)
                s[i] = Console.ReadLine();
            
            Vocales(s, ref cantVocales);
            int total = cantVocales.a + cantVocales.e + cantVocales.i + cantVocales.o + cantVocales.u;

            Console.Out.WriteLine("a = {0} | e = {1} | i = {2} | o = {3} | u = {4}", 
                                    cantVocales.a, cantVocales.e, cantVocales.i, cantVocales.o, cantVocales.u);
            Console.Out.WriteLine("Total de vocales = {0}", total);
            Console.ReadKey();
        }
    }
}
