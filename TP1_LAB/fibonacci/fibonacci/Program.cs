﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        private static void fibonnaci(int n)
        {
            int a = 0, b = 1, c;
            for (int i = 0; i < n; i++)
            {
                Console.Out.Write("{0} ", a);
                c = a + b;
                a = b;
                b = c;
            }
        }
        
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Ingrese un numero limite hasta donde se calculara la serie de Fibonacci:");
            string s = Console.ReadLine();
            int n = 0;

            if(Int32.TryParse(s, out n))
                fibonnaci(n);
            else
                Console.Out.WriteLine("Solo enteros mayor o igual a 1.");

            Console.ReadKey();
        }

        
    }
}
